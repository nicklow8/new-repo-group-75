<!--this page uses function to search from all registered pet data-->

<?php

include_once 'includes/dblovelypets.inc.php'; ?>
<?php include "headsection1.php";?> <!--headsection included through php-->
<?php include "alertmessage.php"; ?> <!--pop up alert messages for users guide-->
	 
<?php

	 function filterTable($query){
            $conn = mysqli_connect("localhost", "root", "", "lovelypets");
			$filter_Result = mysqli_query($conn, $query);
			return $filter_Result;
      }
  

   if (isset($_POST['search'])){
      $valueToSearh = mysqli_real_escape_string($conn, $_POST['valueTosearch']);
      $query = "SELECT * FROM  `users` WHERE CONCAT (`id`, `clinic`, `first`, `last`, `pet`, `email`, `uid`, `pwd`) LIKE '%".$valueToSearh."%' ";
      $search_result = filterTable($query);
      $resultCheck = mysqli_num_rows($search_result);
	  
      if($_POST['search']==""){
         $_SESSION['message']='<p style="background-color:red;  "> No Results Found </p>';
      }
      else{
         $_SESSION['message']= '<p style="background-color:green"> Found Results </p>';
      } 
?>


 <body>
	 
  <div id="content">
	   <h2>User Records</h2></br>
	   
    <table >
     <tr>
        <th>ID</th>
		<th>Clinic</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>E-mail</th>
        <th>Username</th>
        <!--<th>password</th>-->
        <!--<th>Edit</th>-->
        <th>Delete</th>
     </tr>       
    
    <?php

	
        while ($row=mysqli_fetch_assoc($search_result)) {
    
            echo "<tr>";
            echo "<td>".$row['id']."</td>";
			echo "<td>".$row['clinic']."</td>";
            echo "<td>".$row['first']."</td>";
            echo "<td>".$row['last']."</td>";
            echo "<td>".$row['email']."</td>";
            echo "<td>".$row['uid']."</td>";
            //echo "<td>".$row['pwd']."</td>";?>
            <!--<td><a class="update" href="update_pet.php?id=<?php echo $row['id']. 'style="text-decoration:none"' ;?>"><img src="image/update.png" width="30px" height="30px"><br>update</a></td>-->
            <td><a class="delete"  href="delete_users.php?id=<?php echo $row['id'];?>"><img src="image/delete.png" width="30px" height="30px"><br>delete</a></td>
            <?php echo "</tr>";
        } //end while
   }
    ?>  
    </table>
<br/><br/><br/>
      <div class="searchparks">
              <form id="searchparks" name="myFrom" action="pet_edit1.php" method="post" >
               
                  <input type= "text" name="valueTosearch" placeholder="pet search" class="forminput">
                  <button type="submit" class="Submit" name="search"> Search... <img src="image/search.png" width="16px" height="16px"> </button
              </form>    

      </div>  
   </div> 
  </body>   
    <?php include 'footer.php'; ?> <!-- footer included through php-->  
 
</html> 