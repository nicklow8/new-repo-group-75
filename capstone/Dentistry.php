
<?php include "headsection.php" ?>
        
<body>    
      <div id="content" class="left">
        
        <h2>Dentistry</h2><br>
       
        <p style="text-align: justify;"> Approximately 80% of pets have some form of dental disease by the 
        age of four. The most obvious sign to a pet owner is bad breath. Other clinical signs include plaque 
        and tartar build up, reddened gums or gingivitis, excessive salivation, chattering teeth and difficulty 
        chewing food. In reality if your pet has long fur around it's mouth (such as a poodle or maltese) or 
        if you own a cat you will be discussing dental disease with your vet.<br><br>
        Dental disease has been associated to heart, liver and kidney disease in humans and the same links have 
        been evidenced in our pets.  It is well documented that adequate dental care can add up to 4 years to 
        your pet�s life. At Greencross Vets the highest quality dental care is available, utilising dental 
        equipment similar to what you would find at a human dentist. Under a general anaesthetic, your pets 
        teeth, gums and mouth are thoroughly examined, the plaque and tartar is removed and the teeth polished. 
        If you're lucky you might even get a before and after photo!  After the procedure our staff will assist 
        you in constructing a plan to provide at-home dental hygiene aimed at preventing re-occurrence. After 
        all no one likes going to the dentist!<br><br>
        We stock a number of easy-to-feed prescription diets that help prevent tartar re-occurrence. We offer a 
        peace of mind guarantee when you purchase these products, so that if your pet does not find the diet 
        palatable, you can return the food for a complete refund. As well as diets we stock a range of different 
        products to assist in dental home care including toothpaste, toothbrushes, mouth rinses and dental treats.<br><br>
        The appearance and smell of your pet�s mouth after a dental will be amazing and you will be surprised at their 
        new lease on life and improved demeanour once again!</p>
        
            <?php include "trading.php" ?> <!--included trading information-->
            
        </div>
     <?php include "footer.php" ?>     <!-- footer included through php-->
  
</body>
</html>
