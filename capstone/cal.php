<link rel="stylesheet" href="jquery.datetimepicker.css">

<script src="js/jquery-3.2.1.slim.min.js"></script>
<script src="js/jquery.datetimepicker.full.js"></script>

<!-- date Field-->
<input type="text" placeholder="date" id="datetimepicker9"/><br>
	<input type="text" placeholder= "time" id="datetimepicker1"/>

<!--Initializing -->

<script>
    $("#datetimepicker9").datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false,
    format:'d/m/Y',
	formatDate:'Y/m/d'
	
});
    
    $('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:15
});
    
</script>