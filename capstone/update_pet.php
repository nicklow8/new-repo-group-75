<?php
session_start();
include_once 'includes/dblovelypets.inc.php';

$id = $_GET['id'];
$s = "select * from petregistration where id = '$id'";
$m = mysqli_query($conn, $s);
$check = mysqli_fetch_assoc($m);

 
 
if (isset($_POST['submit'])){
    $clinic = mysqli_real_escape_string($conn, $_POST['clinic']);
    $first = mysqli_real_escape_string($conn, $_POST['first']);
    $last = mysqli_real_escape_string($conn, $_POST['last']);
    $pet = mysqli_real_escape_string($conn, $_POST['pet']);
    $dob = mysqli_real_escape_string($conn, $_POST['Date']);
    $type = mysqli_real_escape_string($conn, $_POST['type']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $address = mysqli_real_escape_string($conn, $_POST['address']);
    $gender = mysqli_real_escape_string($conn, $_POST['gender']);
    $message = mysqli_real_escape_string($conn, $_POST['message']);
    
$sql = "UPDATE `petregistration` SET `clinic`='$clinic', `first`= '$first',
`last`='$last',`pet`= '$pet',`Date`= '$dob',`type`='$type',
`email`='$email',`address`='$address',`gender`='$gender',`message`= '$message' WHERE id = '$id'";

$update = mysqli_query ($conn, $sql);
    if(!$update)
        {
             $_SESSION['message'] = '<p style="background-color:red"> Please Fill All The Space Correctly ! </p>' ;    
             header("Location: pagination.php?update=empty");
        }
        else{
			$_SESSION['message'] = '<p style="background-color:green"> Successfully Updated !</p>';
            header("Location: pagination.php?update=success");
        }
        
}        
?>

<?php include "headsection3.php"; ?>


<body>
<div id="content" class="center">
	  		
        <h2>Pet data Update</h2><br>
		
         <div class="searchparks">
	  
              <form id="searchparks" name= "myForm" action="" method="post" >
                  <select class="forminput" name="clinic">
						<option>Please Select clinic</option>
						<option value="Brisbane">Brisbane</option>
						<option value="Gold Coast">Gold Coast</option>
						<option value="Sunshine Coast">Sunshine Coast</option>
						<option value="Toowoomba">Toowoomba</option>
						<option value="Bundaberg">Bundaberg</option>
						<option value="Rockhampton">Rockhampton</option>
				   </select>
                  <input type="text" name="first" value="<?php echo $check['first']; ?>" placeholder="Owner Firstname" class="forminput" >
				  <input type="text" name="last" value="<?php echo $check['last']; ?>" placeholder="Owner Lastname" class="forminput" >
                  <input type="text" name="pet" value="<?php echo $check['pet']; ?>" placeholder="PetName" class="forminput" >
                  <input type= "date" name="Date" value="<?php echo $check['Date']; ?>" placeholder=" Pet dob" class="forminput">
				  <input type= "text" name="type" value="<?php echo $check['type']; ?>" placeholder="Species" class="forminput">
                  <input type= "email" name="email" value="<?php echo $check['email']; ?>" placeholder="E-mail" class="forminput" >
                  <input type= "text" name="address" value="<?php echo $check['address']; ?>" placeholder="Address" class="forminput">
				  
				  <div id="radiobuttons" class="forminput error">
					Male<input type="radio" name="gender" id="male" value="male"> 
					Female<input type="radio" name="gender" id="female" value="female"> 
				  </div>
				  <label id="gender-error" class="error" for="gender"></label>

				  <textarea rows="4" cols="59" name="message" placeholder="medical history/messages" class="forminput"><?php echo $check['message']; ?></textarea>
				  <button type="submit" class="Submit" name="submit">submit</button>
              </form> 
          </div>
      </div>
    </div>
    
<?php include 'footer.php'; ?> <!-- footer included through php-->

</body>
</html>

