<?php include "headsection.php";?> <!--headsection included through php-->

<?php include "alertmessage.php"; ?> <!--pop up alert messages for users guide-->

<body>
     <div id="content" class="center">
      <h2>Book Appointment</h2><br><br>
          <div class="searchparks">
           <form id="searchparks" name= "myForm" action="includes/booking.inc.php" method="post" >
				  
				  <select class="forminput" name="clinic">
						<option>Please Select clinic</option>
						<option value="Brisbane">Brisbane</option>
						<option value="Gold Coast">Gold Coast</option>
						<option value="Sunshine Coast">Sunshine Coast</option>
						<option value="Toowoomba">Toowoomba</option>
						<option value="Bundaberg">Bundaberg</option>
						<option value="Rockhampton">Rockhampton</option>
				   </select>
					
                  <input type="text" name="first" placeholder="Owner Firstname" class="forminput" >
				  <input type="text" name="last" placeholder="Owner Lastname" class="forminput" >
                  <input type="text" name="pet" placeholder="PetName" class="forminput" >
                  <input type= "date" name="Date" placeholder=" Pet dob" class="forminput">
				  <input type= "text" name="type" placeholder="Species" class="forminput">
                  <input type= "email" name="email" placeholder="E-mail" class="forminput" >
                  <input type= "text" name="phonenumber" placeholder="phone number"  class="forminput">
			      <input type="text" name="bookdate" placeholder="date" id="datetimepicker9" class="forminput"/>
	              <input type="text" name="booktime" placeholder= "time" id="datetimepicker1" class="forminput"/>
				  
				  <div id="radiobuttons" class="forminput error">
					Male <input type="radio" name="gender" id="male" value="Male">
					Female <input type="radio" name="gender" id="female" value="Female">
				  </div>
				  <label id="gender-error" class="error" for="gender"></label>
				  
				  <textarea rows="4" cols="59" name="message" placeholder="Booking Reasons" class="forminput"></textarea>
				  <button type="submit" class="Submit" name="submit">submit</button>
				  
              </form>
          </div>
     </div>
	 
 <script>
    $("#datetimepicker9").datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false,
    format:'Y/m/d',
	formatDate:'Y/m/d'
	
});
    
    $('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:15
});
    
</script>
 
    <?php include "footer.php" ?>   <!-- footer included through php-->
    
</body>
</html>