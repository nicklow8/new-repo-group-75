
<?php include "headsection1.php";?> <!--headsection included through php-->

<?php include "alertmessage.php"; ?> <!--pop up alert messages for users guide-->

<body>
   
      <div id="content" class="center">
	  		
        <h2>Pet Registration</h2><br>
		
         <div class="searchparks">
	  
              <form id="searchparks" name= "myForm" action="includes/petregistration.inc.php" method="post" >
				  
				  <select class="forminput" name="clinic">
						<option>Please Select clinic</option>
						<option value="Brisbane">Brisbane</option>
						<option value="Gold Coast">Gold Coast</option>
						<option value="Sunshine Coast">Sunshine Coast</option>
						<option value="Toowoomba">Toowoomba</option>
						<option value="Bundaberg">Bundaberg</option>
						<option value="Rockhampton">Rockhampton</option>
				   </select>
					
                  <input type="text" name="first" placeholder="Owner Firstname" class="forminput" >
				  <input type="text" name="last" placeholder="Owner Lastname" class="forminput" >
                  <input type="text" name="pet" placeholder="PetName" class="forminput" >
                  <input type= "date" name="Date" placeholder=" Pet dob" class="forminput">
				  <input type= "text" name="type" placeholder="Species" class="forminput">
                  <input type= "email" name="email" placeholder="E-mail" class="forminput" >
                  <input type= "text" name="address" placeholder="Address" class="forminput">
				  
				  <div id="radiobuttons" class="forminput error">
					Male <input type="radio" name="gender" id="male" value="Male">
					Female <input type="radio" name="gender" id="female" value="Female">
				  </div>
				  <label id="gender-error" class="error" for="gender"></label>
				  
				  <textarea rows="4" cols="59" name="message" placeholder="medical history/messages" class="forminput"></textarea>
				  <button type="submit" class="Submit" name="submit">submit</button>
				  
              </form>
			  </br>
			  <form id="searchparks" name= "myForm" action="pagination.php" method="post" >
			    <button type="submit" class="Submit" name="submit">view records</button>
			  </form>	
          </div>
      </div>
	
	  <?php include "footer.php" ?>     <!-- footer included through php-->
	  
  
</body>
</html>
