
<?php include "headsection1.php"; ?> <!--headsection and navigation section included through php-->	
	    
<?php include "alertmessage.php"; ?> <!--pop up alert messages for users guide-->		
 
<body>
      <div id="content" class="center">
		
        <h2>Employee Registration</h2><br>
         <div class="searchparks">
                <form id="searchparks" name= "myForm" action="includes/signup.inc.php" method="POST">
				  
				  <select class="forminput" name="clinic">
						<option>Please Select clinic</option>
						<option value="Brisbane">Brisbane</option>
						<option value="Gold Coast">Gold Coast</option>
						<option value="Sunshine Coast">Sunshine Coast</option>
						<option value="Toowoomba">Toowoomba</option>
						<option value="Bundaberg">Bundaberg</option>
						<option value="Rockhampton">Rockhampton</option>
				   </select>
					
				  <input type="text" name="first" placeholder="Firstname" class="forminput" >
				  <input type="text" name="last" placeholder="Lastname" class="forminput" >
				  <input type= "email" name="email" placeholder="E-mail" class="forminput" >
                  <input type="text" name="uid" placeholder="Username" class="forminput">
                  <input type= "password" id="password" name="pwd" placeholder="Password" class="forminput">
				  <input type= "password" name="pwd2" placeholder="confirm password" class="forminput">
				<div id="radiobuttons" class="forminput error">
				  Male <input type="radio" name="gender" id="male">
				  Female <input type="radio" name="gender" id="female">
				</div>
				<label id="gender-error" class="error" for="gender"></label>
              
				  <button type="submit" class="Submit" name="submit">Sign Up</button>
				  
                </form>
            </div>
		 
		    <div id=bottom-img>	
				  <img src="image/cool1.png ">
			</div>	
		 
      </div>
	  

	  <?php include "footer.php" ?>     <!-- footer included through php-->
	  
   
</body>
</html>
