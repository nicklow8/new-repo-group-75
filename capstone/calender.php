<?php include "headsection.php" ?>

        <script>
            //javascript function to go next and previous 
            function goLastMonth(month, year) {
                //for previous function
                if (month == 1) {
                    //if the current 1, decresae the year and set month
                    // to 12
                    --year;
                    month = 13;
                }
                //create "monthstring" variable to store the month in string, then create "monthstring" variable to store the month in string,
                //then create "monthstring" to store the length of the "monthstring", check wheather the monthlength is 1 or lesser, if true then
                //add "0" infront of the monthstring now put the monthstring o the url to state the month
                --month
                var monthstring = ""+month+"";
                var monthlength = monthstring.length;
                if (monthlength <= 1) {
                    monthstring = "0"+monthstring;
                }
                //create url to state the month and year so the checking for month, year will set it to the variable
                document.location.href = "<?php $_SERVER['PHP_SELF'];?>?month="+monthstring+"&year="+year;
            }
            
            function goNextMonth(month, year) {
                //for next function
                if (month == 12) {
                    //if the current 12, increase the year and set month
                    // to 1
                    ++year;
                    month = 0;
                }
                ++month
                var monthstring = ""+month+"";
                var monthlength = monthstring.length;
                if (monthlength <= 1) {
                    monthstring = "0"+monthstring;
                }
                //create url to state the month and year so the checking for month, year will set it to the variable
                document.location.href = "<?php $_SERVER['PHP_SELF'];?>?month="+monthstring+"&year="+year;
            }
            
        </script>
        
    </head>
    <body>
        
         <div id="content" class="center">
            
        <?php // check if day has passing variable
        if(isset($_GET['day'])){
            //if true, get the day fron the url
            $day = $_GET['day'];
        }else{
            //else, set today day
            $day=date("j");
        }
        // check if month has passing variable
        if(isset($_GET['month'])){
            $month = $_GET['month'];   
            }else{
                //else, set today month
                $month = date("n");
            }
        // check if year has passing variable
        if(isset($_GET['year'])){
            $year = $_GET['year'];
        }else{
            //else, set today year
            $year = date("Y");
        }

 
        
        //echo $day."/".$month."/".$year;
        //calender variable
        $currentTimeStamp = strtotime("$year-$month-$day");
        //current month name 
        $monthName = date("F", $currentTimeStamp);
        //get how many days are there in the current month
        $numDays = date("t", $currentTimeStamp);
        //we need to set a variable to count cell in the loop later
        $counter = 0;
        ?>
        
        <!--table on the html page-->
        <div id=table>
        <table border='1'>
            <tr>
                <td><input style='width:50px;' type='button' value='<'name='previousbutton' onclick="goLastMonth(<?php echo $month.",".$year?>)"></td>
                <td colspan='5'><?php echo $monthName. ", ". $year; ?></td><!--display month name year first row on second column-->
                <td><input style='width:50px;' type='button' value='>'name='nextbutton' onclick="goNextMonth(<?php echo $month.",".$year?>)"></td>
            </tr>
            <tr>
                <td width='50px'>Sun</td>
                <td width='50px'>Mon</td>
                <td width='50px'>Tue</td>
                <td width='50px'>Wed</td>
                <td width='50px'>Thu</td>
                <td width='50px'>Fri</td>
                <td width='50px'>Sat</td>
            </tr>
            <?php
            // make a row tag in php 
              echo "<tr>";
                //make a for loop, looping from 1 to number of days in the month
                for($i = 1; $i<$numDays+1; $i++, $counter++){
                    //make timestamp for each day in the loop
                    $timeStamp=strtotime("$year-$month-$i");
                    //make a check if it is 'day 1'
                    if ($i==1){
                        //get which day where 'day 1' fall on
                        $firstDay = date("w", $timeStamp);
                        //make a loop and make a blank cell if it not the first day
                        for($j=0; $j<$firstDay; $j++, $counter++){
                            //blank space
                            echo "<td>&nbsp;</td>";
                        }
                    }
                    //make a check if the day is on the last column if so, we make a new row
                    if($counter % 7 == 0){
                        echo "</tr><tr>";
                    }
                    //add form we need user to select the date they want to view by url...
                    $monthstring = $month;
                    $monthlenght = strlen($monthstring);
                    //since it is a loop day string should get from $i..
                    $daystring = $i;
                    $daylength = strlen($daystring);
                    if($monthlenght<=1){
                        $monthstring = "0".$monthstring;
                    }
                    if($daylength <=1){
                        $daystring = "0".$daystring;
                    }
                    //display the day on the column
                    //we need to pass month, day, year and v (to state the user have selected the date) on the URL on each date..
                    //make a check for the month and day length before setting on the URL 
                    //now under the calender we make a check that user have selected the date..
                    echo "<td align='center'><a href='".$_SERVER['PHP_SELF']."?month=".$monthstring."&day=".$daystring."&year=".$year."&v=true'>".$i."</td>";
                }
                
              echo "</tr>";
            ?>
        </table>
        
        <?php // if user select date is true, we create a text hyperlink "Add Event".the link will need month.day.year , v and f
        //to state user want to add event 
        if(isset($_GET['v'])){
            echo "<a href='".$_SERVER['PHP_SELF']."?month=".$month."&day=".$day."&year=".$year."&v=true&f=true'>Add Event</a>";
            //if user select date is true and user select add form, then include the form into the calender.php page...
            if(isset($_GET['f'])){
                include ("eventform.php");
            }
        }
        ?>
        </div>
        </div>
        <?php include "footer.php" ?>   <!-- footer included through php-->
    </body>
</html>