
<?php include "headsection.php" ?>  <!--includes head section and navigation section-->
	
<body>

      <div id="content" class="left">
        
        <h1>Blood Tests</h1><br>
        <h2>Why do pets need blood tests?</h2>

        <p style="text-align: justify;">Our patients are not able to tell us if they feel unwell, if a fatty meal makes 
        them nauseous or if it hurts when they sit down. While six monthly wellness checks are strongly recommended, a 
        thorough physical exam is not enough to check how our pet&#39;s internal organs are functioning.&nbsp; Blood 
        tests are therefore very important in assisting our veterinarians in diagnosing problems and illnesses in our 
        pets.</p>

        <p style="text-align: justify;">Blood testing on pets is very similar if not the same to those performed on you 
        and me when we visit our doctor or pathologist. With the use of our in-house laboratories and in some instances 
        external laboratories we are able to provide very accurate and timely information about the health of our 
        patients.</p>

        <h2 style="text-align: justify;">What do blood tests test?</h2>

        <p style="text-align: justify;">Blood tests look at the function of the body&#39;s organs, the immune system and 
        the activity or progress of a disease process. Some tests can be used to stimulate or suppress an organ to look 
        into its function more specifically. Some newer tests have the ability to look at your pet&#39;s genetics for 
        specific disease or infections.</p>

        <p style="text-align: justify;">The organs that are tested in the most common series of tests are the liver, 
        kidneys and pancreas.&nbsp; In addition to this certain enzymes are tested which can indicate muscle damage and 
        generalised inflammation.&nbsp; The red &amp; white blood cells are also tested which can give an indication as 
        to hydration status, anaemia, inflammation or infection and how the pet&#39;s immune system is reacting to 
        them.&nbsp;</p>

        <h2 style="text-align: justify;">Why would my pet need a blood test?</h2>

        <p style="text-align: justify;">Pets require blood tests for many reasons. They are used prior to anaesthetics to 
        identify problems that could arise and to adjust medications used for a procedure. They are used to screen for 
        underlying diseases that cannot be picked up by a physical examination or identify diseases common to a specific 
        breed. They can be used for monitoring the effects of certain drug therapies.</p>

        <h2 style="text-align: justify;">How often should pets have a blood test?</h2>

        <p style="text-align: justify;">Depending on the age and condition of your pet the frequency of blood tests 
        can vary. It is recommended that blood tests be performed prior to anaesthetics especially if a problem is 
        suspected. As pets do age much faster than humans (nearly 7 years for every one of ours), a yearly screen for 
        any new developing conditions is highly recommended.</p>

        <h2 style="text-align: justify;">How are blood tests performed?</h2>

        <p style="text-align: justify;">Blood tests are a simple procedure that can be carried out quickly, often during 
        a consultation. Your veterinarian will most often clip a small amount of hair from your pets arm or neck and 
        alcoholic swabs are used to clean the area. A small amount of blood is collected from one of these areas, placed 
        into special tubes and processed either in the clinic or sent to a laboratory. A bandaid may be placed on the arm 
        to stop bruising. Our pets do not seem to feel these needle sticks like we do and before you know it, it&#39;s 
        all over.&nbsp; Occasionally it is required to remove the animal from the consultation room - this is to allow 
        for less distraction to your pet, facilitating a less stressful blood test for all involved!</p>
        
        
			<?php include "trading.php" ?> <!--included trading information-->
        </div>
			<?php include "footer.php" ?> <!--includes footer section-->
   
</body>
</html>
