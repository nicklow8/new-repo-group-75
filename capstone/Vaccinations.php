
<?php include "headsection.php" ?>
	
<body>
  
      <div id="content" class="left">
        
        <h1>Vaccinations</h1><br>
        
        <h2 style="text-align: left;">Why does my pet need vaccinations?</h2>
        
        <p style="text-align: justify;"> Vaccinations are recommended as an essential part of a healthcare program.  Your pets'  'needles' 
        are to protect them from serious and potentially fatal diseases.  Most of these diseases have no targeted 
        cure. Where treatments are available, it can be prolonged and even prove unsuccessful.</p>
        
        <h2 style="text-align: left;">How often does my pet need vaccination?</h2>
        
        <p style="text-align: justify;">Vaccinations rely on stimulating the immune system or put simply activating the body to produce antibodies 
        to the diseases that we want to protect against.  As young animals shouldn't have ever been exposed to to 
        disease they require a series of injections or a "primary course". As adults, it�s important that your 
        veterinarian in consultation with you determines the most suitable long-term vaccination program. 
        Your pet�s lifestyle and environment will be taken into consideration when deciding on their ongoing 
        disease prevention program.</p>
        
        <div>
        <h2>Dogs</h2>
        </div><br>

        <h4>Puppies</h4>

        <div><strong>Core Vaccination schedule: &nbsp;</strong></div>

        <ul>
        	<li>6 - 8 weeks &nbsp;First vaccination&nbsp;</li>
        	<li>10 - 12 weeks &nbsp;Second vaccination</li>
        	<li>14 - 16weeks Third vaccination&nbsp;</li>
        </ul><br>

        <h4>Adult Dogs</h4>

        <p><strong>Ongoing:</strong></p>

        <ul>
        	<li>Dogs require a Canine Cough vaccination every year <em>(mandatory if attending a boarding facility)</em></li>
        </ul><br>

        <h4>Booster Vaccinations</h4><br>

        <div>12 months after your dog&rsquo;s 14 week vaccination another booster vaccine is required, this is to ensure long-term immunity against the contagious diseases. &nbsp;Thereafter, your vet will determine the most suitable ongoing vaccination program. The recommendations outlined by your vet will be guided by your dog&rsquo;s lifestyle and environment.&nbsp;</div>

        <div>&nbsp;</div>
        
        <div>
        <h2>Cats</h2>
        </div><br>

        <h4>Kittens</h4><br>

        <div><strong>Core Vaccination schedule:</strong></div>

        <ul>
        	<li>6 - 8 weeks First vaccination</li>
        	<li>10 - 12 weeks Second vaccination</li>
        	<li>14 - 16 weeks Third booster vaccination</li>
        </ul><br>

        <h4>Adult Cats</h4>

        <div><strong>Ongoing:</strong></div>

        <ul>
        	<li>Cats may require booster vaccinations to ensure long-term immunity against the contagious diseases.</li>
        </ul><br>

        <h4>Feline Immunodeficiency Virus (Feline AIDS):</h4><br>

        <ul>
        	<li>If your cat is exposed to the outdoors or neighbourhood cats, consider incorporating the FIV vaccine into your cat&rsquo;s program. This can be given from 8 weeks of age.</li>
        </ul>

        <p><span style="color: rgb(102, 102, 102); font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 13px; line-height: 21px;">Your veterinarian, in consultation with you, will determine your feline friend&rsquo;s long-term vaccine program. The recommendations will be guided by your cat&rsquo;s lifestyle and environment.</span></p>

        <p style="text-align: justify;">Unvaccinated pets are at risk of contracting serious diseases. You can protect your cat and keep them safe from disease by maintaining the vaccination program recommended by your Greencross Vets pet care team.</p>
                    
               <?php include "trading.php" ?> <!--included trading information-->
        </div>
      <?php include "footer.php" ?>     <!-- footer included through php-->
    
</body>
</html>
