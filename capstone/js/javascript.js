function validate() {
    var returnname = checkname("name");
    var returnpass = checkpassword("pass");
    var returncpass = cpassword();
    var email= validateemail("Email");
    var address = checkaddress("address");
    var state = checkstate();
    var edu = checkedu();

    if (returnname && returnpass && returncpass && email && address && state && edu == true )  {
    return true;
    }
    else{ return false;
    }
}

function checkname() {
    var x = document.getElementById("name").value;
    var regex = /^[A-z ]+$/;

        if (!(regex.test(x))) { document.getElementById("smissing").style.visibility="visible";
            return false;
        }
        else{document.getElementById("smissing").style.visibility="hidden";
            return true;
        }

}

function checkpassword(form) {
    var x = document.getElementById(form).value;
    var regex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (form == "pass"){
    if (!(regex.test(x))) {document.getElementById("pmissing").style.visibility="visible";
        return false;
    }
        else{
            document.getElementById("pmissing").style.visibility="hidden";
            return true;
        }}

}

    function cpassword() {
        var z = document.getElementById("pass").value;
        var y = document.getElementById("cpass").value;
        if (y != z) {
            document.getElementById("cmissing").style.visibility="visible";
            return false;
        }
            else{
                docunment.getElementById("cmissing").style.visibility="hidden";
                return true;
            }
    }
function validateemail(form){
    var x = document.getElementById(form).value;
    var regex = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/;
    if (form == "Email"){
        if ( !(regex.test(x))){
         document.getElementById("missing").style.visibility="visible";
            return false;
        }
            else{
             document.getElementById("missing").style.visibility="hidden";
                return true;
            }
    }
}

function checkaddress(form){
    var x = document.getElementById(form).value;
    var regex = /^[A-z0-9/, ]*$/;
    if (form == "address"){
        if (!(regex.test(x))){
            document.getElementById("amissing").style.visibility="visibility";
        }
    }
}
//alert messages


function success() {
    if (uid===true || pwd===true) {
        //code
        alert("sussfully logged in!");
    }
    else{
        alert("combination of userrname and password incorrect!")
    }
}

function unsuccess() {
    //code
    alert("combination of userrname and password incorrect!")
}

//jQuery valiation

$(function() {

    /*$validator.setDefaults({
        errorPlacement: function(error, element) {
            if (element.prop('type')==='radio') {
                error.insertAfter(element.parent());
            }else{
                error.insertAfter(element);
            }
        }
    });*/

    //strong password function

    $.validator.addMethod('strongPassword', function(value, element) {
        return this.optional(element)
            || value.length >=6
            && /\d/.test(value)
            && /[a-z]/i.test(value);
    }, 'password must be at least 6 characters long and must contain at least one number and one char\'.')

    //name, email and password required function
        $("#searchparks").validate({
            rules:{
                email:{
                    required: true,
                    email: true
                },
                pwd: {
                    required: true,
                    strongPassword: true
                },
                pwd2: {
                    required: true,
                    equalTo: "#password"
                },
                first: {
                    required: true,
                    minlength: 3
                },
                last: {
                    required: true,
                    minlength: 3
                },
                uid: {
                    required: true,
                    minlength: 4
                },
                gender: {
                    required: true,
                },
                Date: {
                    required: true,
                },
                pet: {
                    required: true,
                },
                type: {
                    required: true,
                },
                address:{
                    required: true,
                },
                select:{
                    required: true,
                },
                postcode: {
                    required: true,
                    maxlength: 4,
                    minlength: 4
                },
                phonenumber:{
                    required: true,
                },
                message:{
                    required: true,
                },
                age:{
                    required: true,
                    maxlength: 2,
                    minlength: 1
                },
                position:{
                    required: true,
                }
            },
            messages: {
                first: {
                    required: 'Please enter your firstname',
                    minlength: 'Your username must consist of at least 3',
                },
                last: {
                    required: 'Please enter your lastname',
                    minlength: 'Your username must consist of at least 3',
                },
                uid: {
                    required: 'Please enter a username',
                    minlength: 'Your username must consist of at least 4',
                },
                email:{
                    required: 'Please enter an email address.',
                    email: 'Please enter a <em>valid</em> email address.',
                },

                pwd2: {
                    required: 'Please provide a password',
                    equalTo: 'Please enter the same password as above',
                },
                gender: {
                    required: 'Please choose gender',
                },
                pet: {
                    required: 'Please enter petname',
                    minlength: 'Your username must consist of at least 3',
                },
                Date: {
                    required: 'Please provide date of birth',
                },
                postcode: {
                    required: 'Please enter postcode',
                    maxlength: 'postcode must consist of 4 numbers',
                    minlength: 'postcode must be of 4 digits'
                },
                phonenumber:{
                    required: 'Please enter phonenumber',

                },
                message:{
                    required: 'Please provide information',
                },
                age:{
                    required: 'Please enter your age',
                    maxlength: 'age must consist of 2 numbers',
                    minlength: 'age must be of 1 digits'
                }
                
            }
        });

function popup() {
     var x = document.getElementById("uid").value;
    if (x =="") {
         document.getElementById("alert-error").style.display = "none";
     return true;
    }
   else {
        document.getElementById("alert-error").style.display = "block";
         return false;
   }
}
       
    
// pen availability page..    
 $("select.update-animal").on('change', function(e) {
          var select = $(this);
          var parent = select.parents("tr");
          var id = select.attr("rel");
    $.ajax({
      url: "./includes/manage_collect.php",
      type: "post",
      data: {pet_id: select.val(), pen_id: id},
      success: function(data) {
        console.log("Success");
        console.log(data);
      },
      error: function(err) {
        console.log(err)
      }
    })
        });
        $("select.update-availability").on('change', function(e) {
          var select = $(this);
          var parent = select.parents("tr");
          var id = select.attr("rel");

          $.ajax({
            url: "./includes/manage_availability.php",
            type: "post",
            data: {availability: select.val(), pen_id: id},
            success: function(data) {
              console.log("Success");
              console.log(data);
            },
            error: function(err) {
              console.log(err)
            }

          })

        });

        $("select.update-date-availability").on('change', function(e) {
          var select = $(this);
          var parent = select.parents("tr");
          var id = select.attr("rel");

          $.ajax({
            url: "./includes/change_available_date.php",
            type: "post",
            data: {date: select.val(), pen_id: id},
            success: function(data) {
              console.log("Success");
              console.log(data);
            },
            error: function(err) {
              console.log(err)
            }

          })

        });
});

// booking date and time

 $("#datetimepicker9").datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false,
    format:'d/m/Y',
	formatDate:'Y/m/d'
	
    });
    
    $('#datetimepicker1').datetimepicker({ 
	datepicker:false,
	format:'H:i',
	step:15
    });