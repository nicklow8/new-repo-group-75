
<?php include "headsection.php" ?> <!--includes head section and navigation section-->

<body>
  <div id="content" class="center">
    
      <h2>Contact Lovely Pets</h2>
     
    <div id="map"></div><!--this section puts map on the page using javascript-->
	
	<script>
      function initMap() {
        //map options
        var options = {
            zoom:6,
            center:{lat:-26.3645, lng:152.9677},
        }
        
        //new map
        var map = new google.maps.Map(document.getElementById('map'), options);
      
       // Array of markers //add marker to different location on map 
       var markers = [
            {
                coords:{lat:-27.4698, lng:153.0251},
                content:'<h3>Brisbane</h3><h3>Lovely Pets</h3><p> 1/125 Queen Street, Brisbane, QLD 40000</p>'
            },
                {
                    coords:{lat:-28.0167, lng:153.4000},
                    content:'<h3>Gold Coast</h3><h3>Lovely Pets</h3><p> 1/15 Lothian Avenue, Gold Coast, QLD 40001</p>'
                },
                    {
                        coords:{lat:-26.6500, lng:153.0667},
                        content:'<h3>Sunshine Coast</h3><h3>Lovely Pets</h3><p> 1/83 Maroochy Waters Dr, Sunshine Coast, QLD 40002</p>'
                    },              
                        {
                            coords:{lat:-27.5598, lng:151.9507},
                            content:'<h3>Toowoomba</h3><h3>Lovely Pets</h3><p> 1/12 Station Street, Toowoomba, QLD 40003</p>'
                        },
                            {
                                coords:{lat:-24.8670, lng:152.3510},
                                content:'<h3>Bundaberg</h3><h3>Lovely Pets</h3><p> 1/10 Woongarra Street, Bundaberg, QLD 40004</p>'
                            },
                                {
                                    coords:{lat:-23.3791, lng:150.5100},
                                    content:'<h3>Rockhampton</h3><h3>Lovely Pets</h3><p> 1/5 Alma Ln, Rockhampton, QLD 40005</p>'
                                }
        
        ];
       
       //Loop through markers
       for (var i = 0;i < markers.length; i++) {
        //code //add marker
        addMarker(markers[i]);
       }
       
       
       // add marker function
       function addMarker(props) {
        var marker = new google.maps.Marker({
            position:props.coords,
            map:map
            });
        
        //check content
        if (props.content) {
            //add information when clicked
        var infoWindow = new google.maps.InfoWindow({
            content:props.content
        });
        //add listener to open information when clicked
        marker.addListener('click', function(){
            infoWindow.open(map, marker);
        });
        }
       }
      }
	</script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2LmwNSL4HiF8h7miDzbJ_BLjJOvQq1Sw&callback=initMap">
    </script>
    
    <h3>Head Office</h3>
    
    <p><i>
    Location: 1/125 Queen Streert, Brisbane City QLD 4000<br>
    Telephone:  073455566<br>
          Fax: 073455567
    </i></p>
    <br>
    <hr><br>
    <h2>Email Your Local Clinic</h2><br/>
     
      <div class="searchparks">
       <form id="searchparks" name= "myForm" action="" method="post" >
        
                  <select class="forminput" name="select">
						<option>Please Select clinic</option>
						<option value="Brisbane">Brisbane</option>
						<option value="Gold Coast">Gold Coast</option>
						<option value="Sunshine Coast">Sunshine Coast</option>
						<option value="Toowoomba">Toowoomba</option>
						<option value="Bundaberg">Bundaberg</option>
						<option value="Rockhampton">Rockhampton</option>
				  </select>
         
        <input type="text" name="first" placeholder="First Name" class="forminput"/>
        <input type= "text" name="last" placeholder="Last Name" class="forminput"/>
        <input type= "email" name="email" placeholder="email" class="forminput"/>
        <input type= "number" name="phonenumber" placeholder="phone number" class="forminput"/>
        <textarea rows="4" cols="56" placeholder="message" name="message" class="forminput"></textarea>
        
        <button type="submit" class="Submit">Submit</button>
       </form>
      </div>
    
    
  
   
      <?php include "trading.php" ?> <!--included trading information-->
  
    </div>

      <?php include "footer.php" ?>     <!-- footer included through php-->
  
   </body>
</html>
