<!--this page shows deleted pet data from backuppet-->

<?php include_once 'includes/dblovelypets.inc.php'; ?>

<?php include "headsection1.php";?> <!--headsection included through php-->

<?php include "alertmessage.php"; ?> <!--pop up alert messages for users guide-->

<?php

  //define how many results you want per page
  $result_per_page = 4;
  
  //find out number of results stored in database
  $sql = "SELECT * FROM  `backupinvoice`";
  $result= mysqli_query($conn, $sql);
  $count_pages = mysqli_num_rows($result);
  
  // determine number of total pages available
  $no_of_pages = ceil($count_pages/$result_per_page);

  // determine which page number visitor is currently on
  if (!isset($_GET['page'])){
	  $page = 1;
  }else{
	  $page = $_GET['page'];
  }
  
  // determine the sql LIMIT starting number for the result on the display page
  $this_page_first_result = ($page-1)*$result_per_page;
 
  //retrieve selected results from database and display them on page
  
  $sql = "SELECT * FROM  `backupinvoice` LIMIT " . $this_page_first_result . ',' . $result_per_page;
  $result = mysqli_query($conn, $sql);
  

?>
<body>
   <div id="content">
      
  <h2>Invoice Records</h2></br>
     
    <table >
     <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Medication</th>
        <th>Quantity</th>
        <th>Extra</th>
        <th>Re-Store</th>
     </tr>       
    
    <?php
        while ($row=mysqli_fetch_assoc($result)) {
    
            echo "<tr>";
            echo "<td>".$row['id']."</td>";
            echo "<td>".$row['FirstName']."</td>";
            echo "<td>".$row['LastName']."</td>";
            echo "<td>".$row['Address']."</td>";
            echo "<td>".$row['Email']."</td>";
            echo "<td>".$row['PhoneNumber']."</td>";
            echo "<td>".$row['Medication']."</td>";
            echo "<td>".$row['Quantity']."</td>";
            echo "<td>".$row['Extra']."</td>";  ?>
           <!--<td><a class="update" href="update_pet.php?id=<?php echo $row['id']. 'style="text-decoration:none"' ;?>"><img src="image/update.png" width="30px" height="30px"><br>update</a></td>-->
            <td><a class="restore"  href="restore_invoice.php?id=<?php echo $row['id'];?>"><img src="image/restore1.png" width="30px" height="30px"><br>Re-Store</a></td>
            <?php echo "</tr>";
        } //end while

    ?>  
    </table>
	
  <?php
  
  // shows thepage number links...
	  for($b=1;$b<=$no_of_pages;$b++){
		 ?><a href="invoice_table.php?page=<?php echo $b ?>" style="text-decoration:none " ><?php echo $b." "; ?></a><?php
	  }
  
   ?>
  
  </br></br></br>
    
	
   
      <div class="searchparks">
              <form id="searchparks" name="myFrom" action="search_invoice_deleted.php" method="post" >
               
                  <input type= "text" name="valueTosearch" placeholder="invoice search" class="forminput">
                  <button type="submit" class="Submit" name="search"> Search... <img src="image/search.png" width="16px" height="16px"> </button
              </form>    
  
      </div>  
   </div> 
 </body>   
    <?php include 'footer.php'; ?> <!-- footer included through php-->  
 
</html> 
