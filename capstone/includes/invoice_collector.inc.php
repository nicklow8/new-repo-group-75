<?php

session_start();
$_SESSION['message'] = '';

if (isset($_POST['submit'])){
    
   include_once 'dblovelypets.inc.php';

    $FirstName = mysqli_real_escape_string($conn, $_POST['FirstName']);
    $LastName = mysqli_real_escape_string($conn, $_POST['LastName']);
    $Address = mysqli_real_escape_string($conn, $_POST['Address']);
    $Email = mysqli_real_escape_string($conn, $_POST['Email']);
    $PhoneNumber = mysqli_real_escape_string($conn, $_POST['PhoneNumber']);
    $Medication = mysqli_real_escape_string($conn, $_POST['Medication']);
    $Quantity = mysqli_real_escape_string($conn, $_POST['Quantity']);
    $Extra = mysqli_real_escape_string($conn, $_POST['Extra']);
    
     //Error handlers
    //Check for empty fields
    if (empty($FirstName) ||  empty($LastName) || empty($Address) || empty($Email) || empty($PhoneNumber) ||empty($Medication) || empty($Quantity) || empty($Extra)){
    $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill All The Space Correctly </p>';    
    header("Location: ../invoice collector.php?invoice collector=empty");
    exit(); 
    }else{
        //check if input characters are valid
        if (!preg_match("/^[a-zA-Z]*$/", $FirstName) || !preg_match("/^[a-zA-Z]*$/", $LastName)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill All The Space Properly </p>'; 
            header("Location: ../invoice collector.php?invoice collector=invalidname");
            exit();
        }else{
            //Check if email is valid
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill Valid Email </p>';     
            header("Location: ../invoice collector.php?invoice collector=invalidemail");
            exit();
            }else{
                //Insert the pet registration into the database
                    $sql = "INSERT INTO invoice (FirstName, LastName, Address, Email, PhoneNumber, Medication, Quantity, Extra) VALUES ('$FirstName', '$LastName', '$Address', '$Email', '$PhoneNumber', '$Medication',
                    '$Quantity', '$Extra');";
                    $result = mysqli_query($conn, $sql);
                    $_SESSION['message'] = '<p style="background-color:green;"> Registration Successfull ! </p>';
                     header("Location: ../invoice collector.php?invoice collector=success");
                     exit();
            }
        }
    }
}else{
    header("Location: ../invoice collector.php");
    exit();
}    
?>