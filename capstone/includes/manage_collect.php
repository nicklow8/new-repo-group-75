<?php
// this page is for searching for clinics
  include_once './dbh.inc.php';
  global $conn;

try {
    // $pensId = "A1";

    // $petId = "Dog";
    $petId = $_POST['pet_id'];
    $pensId = $_POST['pen_id'];
    $query = "UPDATE pens_allocation SET pet_id = ? WHERE pens_id = ?";
    $statement = $conn->prepare($query);
    $statement->bind_param("ss", $petId, $pensId);
    $statement->execute();
    $statement->store_result();
    // echo $statement->error;
    echo $statement->affected_rows;

} catch(Exception $e) {
    echo $e->message();
}
?>
