<?php

session_start();
$_SESSION['message'] = '';

if (isset($_POST['submit'])){
    
    include_once 'dblovelypets.inc.php';
    $Clinic = mysqli_real_escape_string($conn, $_POST['clinic']);
    $FirstName = mysqli_real_escape_string($conn, $_POST['first']);
    $LastName = mysqli_real_escape_string($conn, $_POST['last']);
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $Age = mysqli_real_escape_string($conn, $_POST['age']);
    $Gender = mysqli_real_escape_string($conn, $_POST['gender']);
    $Position = mysqli_real_escape_string($conn, $_POST['position']);
    $Address = mysqli_real_escape_string($conn, $_POST['address']);
    $Email = mysqli_real_escape_string($conn, $_POST['email']);
    $PhoneNumber = mysqli_real_escape_string($conn, $_POST['phonenumber']);
    $Extra = mysqli_real_escape_string($conn, $_POST['message']);
    
     //Error handlers
    //Check for empty fields
    if(empty($Clinic) || empty($FirstName) ||  empty($LastName) || empty($uid) || empty($Age) || empty($Gender) || empty($Position) ||empty($Address) || empty($Email) || empty($PhoneNumber) || empty($Extra)){
   $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill All The Space Correctly </p>';    
    header("Location: ../Staff edit.php?Staff edit=empty");
    exit(); 
    }else{
        //check if input characters are valid
        if (!preg_match("/^[a-zA-Z]*$/", $FirstName) || !preg_match("/^[a-zA-Z]*$/", $LastName)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill All The Space Properly </p>'; 
            header("Location: ../Staff edit.php?Staff edit=invalidname");
            exit();
        }else{
            //Check if email is valid
            if(!filter_var($Email, FILTER_VALIDATE_EMAIL)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill Valid Email </p>';     
            header("Location: ../Staff edit.php?Staff edit=invalidemail");
            exit();
            }else{
                //Insert the pet registration into the database
                    $sql = "INSERT INTO staffv1 (clinic, first, last, uid, age, gender, position, address, email, phonenumber, message) VALUES ('$Clinic', '$FirstName', '$LastName', '$uid', '$Age', '$Gender', '$Position', '$Address',
                    '$Email', '$PhoneNumber', '$Extra');";
                    $result = mysqli_query($conn, $sql);
                    $_SESSION['message'] = '<p style="background-color:green;"> Registration Successfull ! </p>';
                     header("Location: ../Staff edit.php?Staff edit=success");
                     exit();
            }
        }
    }
}else{
    header("Location: ../Staff edit.php");
    exit();
}    
?>