<?php
// this page is for searching for clinics
  include_once './dbh.inc.php';
  global $conn;

try {
    // $pensId = "A1";
    // $petId = "Dog";
    $availability = $_POST['availability'];
    if ($availability == "Available") {
    $date = $_POST['date'];
    } else {
      $date = gmdate('Y-m-d h:i:s');
    }
    $pensId = $_POST['pen_id'];
    $query = "UPDATE pens_allocation SET pen_availability = ?, date_available = ? WHERE pens_id = ?";
    $statement = $conn->prepare($query);
    $statement->bind_param("sss", $availability, $date , $pensId);
    $statement->execute();
    $statement->store_result();
    // echo $statement->error;
    echo $statement->affected_rows;

} catch(Exception $e) {
    echo $e->message();
}
?>
