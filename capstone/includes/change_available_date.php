<?php
// this page is for searching for clinics
  include_once './dbh.inc.php';
  global $conn;

try {
    // $pensId = "A1";

    // $petId = "Dog";
    $date = $_POST['date'];
    $pensId = $_POST['pen_id'];
    $query = "UPDATE pens_allocation SET date_available = ? WHERE pens_id = ?";
    $statement = $conn->prepare($query);
    $statement->bind_param("ss", $date, $pensId);
    $statement->execute();
    $statement->store_result();
    // echo $statement->error;
    echo $statement->affected_rows;

} catch(Exception $e) {
    echo $e->message();
}
?>
