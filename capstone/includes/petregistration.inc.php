<?php

session_start();
$_SESSION['message'] = '';

if (isset($_POST['submit'])){
    
   include_once 'dblovelypets.inc.php';
    $clinic = mysqli_real_escape_string($conn, $_POST['clinic']);
    $first = mysqli_real_escape_string($conn, $_POST['first']);
    $last = mysqli_real_escape_string($conn, $_POST['last']);
    $pet = mysqli_real_escape_string($conn, $_POST['pet']);
    $dob = mysqli_real_escape_string($conn, $_POST['Date']);
    $type = mysqli_real_escape_string($conn, $_POST['type']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $address = mysqli_real_escape_string($conn, $_POST['address']);
    $gender = mysqli_real_escape_string($conn, $_POST['gender']);
    $message = mysqli_real_escape_string($conn, $_POST['message']);
    
     //Error handlers
    //Check for empty fields
    if(empty($clinic) || empty($first) ||  empty($last) || empty($pet) || empty($dob) || empty($type) ||empty($email) || empty($address) || empty($gender) || empty($message)){
    $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill All The Space Correctly </p>';    
    header("Location: ../registration.php?registration=empty");
    exit(); 
    }else{
        //check if input characters are valid
        if (!preg_match("/^[A-Za-z]*$/", $first) || !preg_match("/^[a-zA-Z]*$/", $last)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill All The Space Properly </p>'; 
            header("Location: ../registration.php?registration=invalidname");
            exit();
        }else{
            //Check if email is valid
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill Valid Email </p>';     
            header("Location: ../registration.php?registration=invalidemail");
            exit();
            }else{
               //Check date
               //if(!checkdate($dob, $month, $day, $year)){
               //$_SESSION['message'] = '<p style="background-color:red;  ">Please Fill Valid Date </p>';     
               //header("Location: ../registration.php?registration=invaliddate");
               //exit();
               //}else{
                //Insert the pet registration into the database
                    $sql = "INSERT INTO petregistration (clinic, first, last, pet, Date, type, email, address, gender, message) VALUES ('$clinic', '$first', '$last', '$pet', '$dob', '$type', '$email',
                    '$address', '$gender', '$message');";
                    $result = mysqli_query($conn, $sql);
                    $_SESSION['message'] = '<p style="background-color:green;"> Registration Successfull ! </p>';
                     header("Location: ../registration.php?registration=success");
                     exit();
                    
            }
         }
      }
}else{
    header("Location: ../registration.php");
    exit();
}    
?>