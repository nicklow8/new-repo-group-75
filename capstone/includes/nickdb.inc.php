<?php
define("database_HOST", "localhost");
define("database_USER", "root");
define("database_PASS", "");
define("database_NAME", "lovelypets");

// Establish connection to the database using PDO
try {
    $database = new PDO("mysql:host=" . database_HOST . ";dbname=" . database_NAME, database_USER, database_PASS);
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
    exit();
}
