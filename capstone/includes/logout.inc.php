

<?php

if (isset($_POST['submit'])){
    session_start();
    session_unset();
    session_destroy();
    $_SESSION['message']='<p style="background-color:green;">you have successfully logged out !</p>'; 
    header("Location: ../login.php");
    exit();
}
?>