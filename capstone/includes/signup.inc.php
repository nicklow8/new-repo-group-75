<?php
session_start();
if (isset($_POST['submit'])){
    
    include_once 'dblovelypets.inc.php';
    $_SESSION['message'] = '';
    $_SESSION['emessage'] = '';
    $clinic = mysqli_real_escape_string($conn, $_POST['clinic']);
    $first = mysqli_real_escape_string($conn, $_POST['first']);
    $last = mysqli_real_escape_string($conn, $_POST['last']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);
    
    //Error handlers
    //Check for empty fields
    if (empty($clinic) || empty($first) ||  empty($last) || empty($email) || empty($uid) || empty($pwd)){
    $_SESSION['message'] = '<p style="background-color:red;  "> Please Fill All The Empty Space </p>';    
    header("Location: ../signup.php?signup=empty");    
    exit();    
    }else{
        //check if input characters are valid
        if (!preg_match("/^[a-zA-Z]*$/", $first) || !preg_match("/^[a-zA-Z]*$/", $last)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill All The Empty Space Properly </p>';  
            header("Location: ../signup.php?signup=invalidname");
            exit();
        }else{
            //Check if email is validf
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $_SESSION['message'] = '<p style="background-color:red;  ">Please Fill Valid Email </p>';   
            header("Location: ../signup.php?signup=invalidemail");
            exit();
            }else{
                $sql = "SELECT * FROM users WHERE uid='$uid'";
                $result = mysqli_query($conn, $sql);
                $resultCheck = mysqli_num_rows($result);
                
                if($resultCheck > 0){
                     $_SESSION['message'] = '<p style="background-color:red;  "> User Already Taken </p>';
                     header("Location: ../signup.php?signup=usertaken");
                     exit();
                }else{
                    //Hashing the password
                    $hashedPwd = password_hash($pwd, PASSWORD_BCRYPT);
                   
                    //Insert the user into the database
                    $sql = "INSERT INTO users (clinic, first, last, email, uid, pwd ) VALUES ('$clinic', '$first', '$last', '$email',
                    '$uid', '$hashedPwd' )";
                    $result = mysqli_query($conn, $sql);
                    $_SESSION['message'] = '<p style="background-color:green;"> Registration Successfull !</p>';
                    header("Location: ../signup.php?signup=success");
                    exit();
                }
            }
        }
    }
    
}else{
    header("Location: ../signup.php");
    exit();
}

?>