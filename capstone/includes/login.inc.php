<?php

session_start();

if (isset($_POST['submit'])){
    include 'dblovelypets.inc.php';
    $_SESSION['message'] = '';
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);
    
    //Error handlers
    //Check if inputs are empty
    if (empty($uid) || empty($pwd)){
		$_SESSION['message']= '<p style="background-color:red;"> please fill the form </p>';
        header("Location: ../login.php?login=empty");
		exit();
	} else {
		$sql = "SELECT * FROM users WHERE uid='$uid' OR email='$uid'";
		$result = mysqli_query($conn, $sql);
		$resultCheck = mysqli_num_rows($result);
		if ($resultCheck < 1) {
			$_SESSION['message']='<p style="background-color:red;"> combination of username and password incorrect </p>' ;
			header("Location: ../login.php?login=user id or password incorrect");
			exit();
		} else {
			if ($row = mysqli_fetch_assoc($result)) {
				//De-hashing the password
				$hashedPwdCheck = password_verify($pwd, $row['pwd']);
				if ($hashedPwdCheck == false) {
				$_SESSION['message'] =' <p style="background-color:red;"> Wrong password combination! </p>';
					header("Location: ../login.php?login=user id or password incorrect");
					exit();
				} elseif ($hashedPwdCheck == true) {
					//Log in the user here
					$_SESSION['u_id'] = $row['id'];
					$_SESSION['u_first'] = $row['first'];
					$_SESSION['u_last'] = $row['last'];
					$_SESSION['u_email'] = $row['email'];
					$_SESSION['u_uid'] = $row['uid'];
					$_SESSION['message'] = '<p style="background-color:green;">("Welcome" .$row["user_first"] ." ".$row["user_last"] )</p>';
					header("Location: ../home3.php?login=success");
					exit();
				}
			}
		}
	}
} else {
	header("Location: ../login.php?login=error");
	exit();
	}
?>