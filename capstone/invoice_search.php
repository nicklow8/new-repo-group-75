<?php

include_once 'includes/dblovelypets.inc.php'; ?>
<?php include "headsection1.php";?> <!--headsection included through php-->
<?php include "alertmessage.php"; ?> <!--pop up alert messages for users guide-->
	 
<?php	 
	 
	 
	 function filterTable($query){
            $conn = mysqli_connect("localhost", "root", "", "lovelypets");
			$filter_Result = mysqli_query($conn, $query);
			return $filter_Result;
      }
  

   if (isset($_POST['search'])){
      $valueToSearh = mysqli_real_escape_string($conn, $_POST['valueTosearch']);
      $query = "SELECT * FROM  `invoice` WHERE CONCAT (`id`, `FirstName`, `LastName`
      , `Email`, `Medication`, `Quantity`, `Extra`) LIKE '%".$valueToSearh."%' ";
      $search_result = filterTable($query);
      $resultCheck = mysqli_num_rows($search_result);
	  
      if($_POST['search']==""){
         $_SESSION['message']='<p style="background-color:red;  "> No Results Found </p>';
      }
      else{
         $_SESSION['message']= '<p style="background-color:green"> Found Results </p>';
      } 
?>


 <body>
  <div id="content">
	   <h2>Invoice Records</h2></br>
	   
    <table >
     <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Medication</th>
        <th>Quantity</th>
        <th>Extra</th>
        <th>Edit</th>
        <th>Delete</th>
     </tr>       
    
    <?php

	
        while ($row=mysqli_fetch_assoc($search_result)) {
    
            echo "<tr>";
            echo "<td>".$row['id']."</td>";
            echo "<td>".$row['FirstName']."</td>";
            echo "<td>".$row['LastName']."</td>";
            echo "<td>".$row['Address']."</td>";
            echo "<td>".$row['Email']."</td>";
            echo "<td>".$row['PhoneNumber']."</td>";
            echo "<td>".$row['Medication']."</td>";
            echo "<td>".$row['Quantity']."</td>";
            echo "<td>".$row['Extra']."</td>";  ?>
            <td><a class="update" href="invoice_update.php?id=<?php echo $row['id']. 'style="text-decoration:none"' ;?>"><img src="image/update.png" width="30px" height="30px"><br>update</a></td>
            <td><a class="delete"  href="invoice_delete.php?id=<?php echo $row['id'];?>"><img src="image/delete.png" width="30px" height="30px"><br>delete</a></td>
            <?php echo "</tr>";
        } //end while
   }
    ?>  
    </table>
<br/><br/><br/>
      <div class="searchparks">
              <form id="searchparks" name="myFrom" action="invoice_search.php" method="post" >
               
                  <input type= "text" name="valueTosearch" placeholder="invoice search" class="forminput">
                  <button type="submit" class="Submit" name="search"> Search... <img src="image/search.png" width="16px" height="16px"> </button
              </form>    

      </div>  
   </div> 
  </body>   
    <?php include 'footer.php'; ?> <!-- footer included through php-->  
 
</html> 