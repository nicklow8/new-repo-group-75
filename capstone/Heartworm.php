
<?php include "headsection.php" ?>

<body>

      <div id="content" class="left">
        
        <h2>Heartworm</h2><br>
       
        <h3>What is heartworm?</h3>
        <p style="text-align: justify;">Heartworm is a parasitic worm that is transmitted by mosquitos &amp; 
        infects dogs and cats. An infected mosquito bites the pet and injects a larval stage of the worm under 
        the skin. This larval stage matures in the pet&rsquo;s organs for 5 to 6 months becoming an adult worm 
        and migrating through the organs to the heart and blood vessels of the lungs. These adult worms then 
        breed to produce microfilaria (baby heart worm) in the bloodstream which are then drawn up by a mosquito 
        when it feeds on the pet. And so the cycle begins again.</p>
		
        <p style="text-align: justify;">Unfortunately as we are all aware that mosquitoes can find their way into 
		most houses, fly screens or not, this means that even completely indoor pets are at risk of infection 
		from heartworm.</p>
		
		<h2 style="text-align: justify;">What are the signs of heartworm?</h2>
		<h4>DOGS</h4>
		<p style="text-align: justify;">Initially few signs of infection may be seen. Heartworm is usually an 
		insidious slow onset disease. Months or years may pass before signs are seen. When symptoms do appear, 
		they are usually signs of heart failure. The worms interfere with the movement of the heart valves as 
		well as causing turbulence of the blood and roughening in the blood vessels going to the lungs. The heart 
		has to work much harder to pump the blood, and starts to become enlarged &amp; exhausted. The earliest signs 
		may be shortness of breath, loss of stamina or a nagging dry cough. As the disease progresses, breathing 
		becomes more difficult, the abdomen may distend with fluid and the dog becomes lethargic, loses weight, and 
		often stops eating. Often one of the only symptoms is a severe coughing attack leading to coughing up large 
		amounts of blood and fatal haemorrhage. If left untreated, heartworm is nearly always fatal!</p>

		<h4>CATS</h4>
		<p style="text-align: justify;">Usually, there are few clinical signs in the cat. Heartworm may be associated 
		with heart failure, a cough, vague lethargy, but usually the main sign is sudden death, sometimes after the cat 
		coughs up blood. One or two heartworm infecting a cat are normally enough to be fatal!</p>
		
		<h2 style="text-align: justify;">How do I know if my pet has heartworm?</h2>

		<p style="text-align: justify;">A blood test is performed at the vet clinic. In the case of dogs, the test is 
		very accurate, and results are often available before you leave the clinic. Because of the long breeding cycle 
		of the heartworms two tests six months apart may be required to give your dog the &#39;all clear&#39;.</p>

		<p style="text-align: justify;">For cats, testing is less accurate because of the lower number of worms involved. 
		False negatives are common when cats are tested for heartworm. An ultrasound examination may be required to 
		confirm an infection.</p>

		<h2 style="text-align: justify;">How common is heartworm?</h2>

		<p style="text-align: justify;">Heartworm is very common in Brisbane. Over 50% of non-medicated dogs will become 
		infected with heartworm! Infection rates in cats are uncertain. Figures varying from 2-5% have been established, 
		making it a very significant disease of cats.&nbsp;</p>
		
		<h2 style="text-align: justify;">Can heartworm be treated?</h2>

		<h4>DOGS</h4>

		<p style="text-align: justify;">Yes! However, treatment is not without its potential problems. The immiticide 
		used can cause allergic reactions in some dogs, requires a long treatment course and ongoing follow-up medications 
		to prevent potential side effects.&nbsp;&nbsp; Prevention is far better than treatment.</p>

		<h4>CATS</h4>

		<p style="text-align: justify;">Treatment is difficult and is usually very complex surgical extraction.&nbsp; 
		There are no drugs approved for treatment of heartworm in cats, so prevention is far better.</p>

		<h2 style="text-align: justify;">How do I&nbsp;prevent my pet getting heartworm?</h2>

		<h4>DOGS</h4>

		<p style="text-align: justify;">If your dog is over 6 months old, a blood test is necessary before you commence 
		prevention. Prevention should begin at 6 to 8 weeks of age, and is achieved by a number of different ways. Daily 
		medication comes in a tablet and a liquid form, and must be given each and every day to be effective. It is the 
		cheapest form of prevention, but is also the most prone to failure. Monthly medication comes in either tablet or 
		top-spot preparation and most of these can help protect your pet against intestinal worms and even fleas as well!</p>

		<p style="text-align: justify;">By far the most convenient heart worm prevention is the yearly injection 
		administered by your veterinarian.&nbsp;</p>

		<h4>CATS</h4>

		<p style="text-align: justify;">Monthly tablets or top-spots are available for cats to prevent heartworm infection. 
		These products also control gastrointestinal worms and some products also control fleas.&nbsp;<br />
		<br />
		Remember if you get bitten by mosquitoes, there is a risk that your pet could become infected with heartworm 
		through a mosquito bite.&nbsp; The best treatment is definitely prevention.&nbsp;<br />
        
         <?php include "trading.php" ?> <!--included trading information-->
        </div>
       <?php include "footer.php" ?> 
    
</body>
</html>
