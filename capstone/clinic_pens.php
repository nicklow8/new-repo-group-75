﻿
<?php include "headsection1.php" ?>

<body>
      <div id="content" class="center">

        <h2>Clinic Pens</h2> <br><br>
            <h3>Available Pens</h3><br>
               <h4>Search and Manage Availability for Clinic Pens</h4><br>

              <div class="searchparks">
                <form id="searchparks" name= "myForm" action="manage_pens.php" method="post">
                 <select name = "clinic" class="forminput" >
                  <option value = "none">Please Select</option>
                  <option value = "Brisbane">Brisbane</option>
                  <option value = "Bundaberg">Bundaberg</option>
                  <option value = "Gold Coast">Gold Coast</option>
                  <option value = "Rockhampton">Rockhampton</option>
                  <option value = "Sunshine Coast">Sunshine Coast</option>
                  <option value = "Toowoomba">Toowoomba</option>
                 </select>
                  <button type="submit" class="Submit" name="search" id="search" value="Sign">Search</button>
                </form>
              </div>
        </div>
        <?php include "footer.php" ?>     <!-- footer included through php-->
    
</body>
</html>
