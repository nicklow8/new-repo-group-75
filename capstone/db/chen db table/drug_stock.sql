-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017-10-16 19:42:51
-- 服务器版本： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lovelypets`
--

-- --------------------------------------------------------

--
-- 表的结构 `drug_stock`
--

CREATE TABLE `drug_stock` (
  `ID` int(20) NOT NULL,
  `ClinicName` varchar(45) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Quantity` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `drug_stock`
--

INSERT INTO `drug_stock` (`ID`, `ClinicName`, `Name`, `Quantity`) VALUES
(1, 'lovelypetsA', 'Doxycycline', 500),
(2, 'lovelypetsA', 'Enalapril', 300),
(3, 'lovelypetsB', 'Firocoxib', 300),
(4, 'lovelypetsA', 'Griseofulvin', 500),
(5, 'lovelypetsB', 'Hydralazine', 0),
(6, 'lovelypetsC', 'Kaopectate', 60),
(7, 'lovelypetsC', 'Enalapril', 200);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drug_stock`
--
ALTER TABLE `drug_stock`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `drug_id_UNIQUE` (`ID`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `drug_stock`
--
ALTER TABLE `drug_stock`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
