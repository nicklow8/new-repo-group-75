-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017-10-16 19:42:44
-- 服务器版本： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lovelypets`
--

-- --------------------------------------------------------

--
-- 表的结构 `drug_purchase`
--

CREATE TABLE `drug_purchase` (
  `ID` int(20) UNSIGNED NOT NULL,
  `ClinicName` varchar(45) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Quantity` int(10) UNSIGNED NOT NULL,
  `Price` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `drug_purchase`
--

INSERT INTO `drug_purchase` (`ID`, `ClinicName`, `Name`, `Quantity`, `Price`, `date`) VALUES
(1, 'lovelypetsA', 'Firocoxib', 200, 50, '2017-03-03'),
(2, 'lovelypetsB', 'Firocoxib', 100, 40, '2017-05-04'),
(3, 'lovelypetsA', 'Griseofulvin', 50, 200, '2017-01-24'),
(4, 'lovelypetsB', 'Griseofulvin', 300, 300, '2017-02-11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drug_purchase`
--
ALTER TABLE `drug_purchase`
  ADD PRIMARY KEY (`ID`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `drug_purchase`
--
ALTER TABLE `drug_purchase`
  MODIFY `ID` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
