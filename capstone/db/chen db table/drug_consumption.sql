-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017-10-16 19:42:35
-- 服务器版本： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lovelypets`
--

-- --------------------------------------------------------

--
-- 表的结构 `drug_consumption`
--

CREATE TABLE `drug_consumption` (
  `ID` int(20) NOT NULL,
  `ClinicName` varchar(45) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Quantity` int(10) UNSIGNED NOT NULL,
  `Date` date NOT NULL,
  `Description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `drug_consumption`
--

INSERT INTO `drug_consumption` (`ID`, `ClinicName`, `Name`, `Quantity`, `Date`, `Description`) VALUES
(1, 'lovelypetsA', 'Doxycycline', 1, '2017-08-11', 'AAA'),
(2, 'lovelypetsA', 'Doxycycline', 5, '2017-07-11', 'BBB'),
(3, 'lovelypetsB', 'Firocoxib', 2, '2017-08-12', 'CCC'),
(4, 'lovelypetsC', 'Firocoxib', 3, '2017-08-10', 'DDD');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drug_consumption`
--
ALTER TABLE `drug_consumption`
  ADD PRIMARY KEY (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
