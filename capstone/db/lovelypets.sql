-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2017 at 04:58 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lovelypets`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `appointment_id` int(20) UNSIGNED NOT NULL,
  `staff_id` int(20) NOT NULL,
  `pet_id` int(20) NOT NULL,
  `appointment_starttime` datetime NOT NULL,
  `appointment_description` varchar(120) NOT NULL,
  `appointment_state` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`appointment_id`, `staff_id`, `pet_id`, `appointment_starttime`, `appointment_description`, `appointment_state`) VALUES
(5001, 100001, 200003, '2017-08-11 00:00:00', 'AA1', 0),
(5002, 100002, 200002, '2017-03-15 00:00:00', 'AA2', 0),
(5003, 100003, 200004, '2017-08-13 00:00:00', 'AA3', 0),
(5005, 100003, 200001, '2017-09-12 00:00:00', 'AAA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `book_appointment`
--

CREATE TABLE `book_appointment` (
  `book_appointment_id` int(20) UNSIGNED NOT NULL,
  `petowner_id` int(20) NOT NULL,
  `pet_id` int(20) NOT NULL,
  `book_message` varchar(120) NOT NULL,
  `petowner_phonenumber` int(25) NOT NULL,
  `book_status` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_appointment`
--

INSERT INTO `book_appointment` (`book_appointment_id`, `petowner_id`, `pet_id`, `book_message`, `petowner_phonenumber`, `book_status`) VALUES
(900001, 20001, 200001, 'AAA', 41310000, 0),
(900002, 20002, 200003, 'BBBB', 44444, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clinic`
--

CREATE TABLE `clinic` (
  `clinic_id` int(20) NOT NULL,
  `clinic_name` varchar(45) NOT NULL,
  `clinic_address` varchar(120) NOT NULL,
  `clinic_contactn number` int(20) UNSIGNED NOT NULL,
  `clinic_opentime` time NOT NULL,
  `clinic_closetime` time NOT NULL,
  `clinic_email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clinic`
--

INSERT INTO `clinic` (`clinic_id`, `clinic_name`, `clinic_address`, `clinic_contactn number`, `clinic_opentime`, `clinic_closetime`, `clinic_email`) VALUES
(300001, 'LOVEPETS1', 'PET ROAD 100', 414200000, '08:00:00', '18:00:00', '0414200000@gmail.com'),
(300002, 'LOVEPETS2', 'PET STREET 100', 414300000, '09:00:00', '18:00:00', '0414300000@gmail.com'),
(300003, 'LOVEPETS3', 'PET AREA 200', 414400000, '10:00:00', '20:00:00', '0414400000@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `clinic_staff`
--

CREATE TABLE `clinic_staff` (
  `staff_id` int(20) NOT NULL,
  `clinic_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clinic_staff`
--

INSERT INTO `clinic_staff` (`staff_id`, `clinic_id`) VALUES
(100001, 300001),
(100002, 300002),
(100003, 300003),
(100004, 300003),
(100005, 300002),
(100006, 300001),
(100007, 300001);

-- --------------------------------------------------------

--
-- Table structure for table `drug_consumption`
--

CREATE TABLE `drug_consumption` (
  `drug_id` int(20) NOT NULL,
  `clinic_id` int(20) NOT NULL,
  `treatment_id` int(20) NOT NULL,
  `drug_number` int(10) UNSIGNED NOT NULL,
  `drug_consumption_date` datetime NOT NULL,
  `comsumption_description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drug_consumption`
--

INSERT INTO `drug_consumption` (`drug_id`, `clinic_id`, `treatment_id`, `drug_number`, `drug_consumption_date`, `comsumption_description`) VALUES
(400001, 300001, 500100, 1, '2017-08-11 00:00:00', 'AAA'),
(400003, 300001, 500101, 5, '2017-07-11 00:00:00', 'BBB'),
(400005, 300002, 500101, 2, '2017-08-12 00:00:00', 'CCC'),
(400005, 300003, 500100, 3, '2017-08-10 00:00:00', 'DDD');

-- --------------------------------------------------------

--
-- Table structure for table `drug_inventory`
--

CREATE TABLE `drug_inventory` (
  `drug_id` int(20) NOT NULL,
  `clinic_id` int(20) NOT NULL,
  `drug_name` varchar(45) NOT NULL,
  `drug_number` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `drug_sell_price` int(10) UNSIGNED NOT NULL,
  `drug_description` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drug_inventory`
--

INSERT INTO `drug_inventory` (`drug_id`, `clinic_id`, `drug_name`, `drug_number`, `drug_sell_price`, `drug_description`) VALUES
(400001, 300001, 'Doxycycline', 944, 300, NULL),
(400002, 300001, 'Enalapril', 300, 50, NULL),
(400003, 300002, 'Firocoxib', 300, 20, NULL),
(400004, 300001, 'Griseofulvin', 500, 30, NULL),
(400005, 300003, 'Hydralazine', 0, 200, NULL),
(400006, 300002, 'Kaopectate', 60, 90, NULL),
(400007, 300002, 'Enalapril', 200, 48, NULL),
(400008, 300001, 'AAAA', 2000, 200, 'AAAAA');

-- --------------------------------------------------------

--
-- Table structure for table `drug_purchase`
--

CREATE TABLE `drug_purchase` (
  `drug_purchase_id` int(11) NOT NULL,
  `drug_id` int(20) NOT NULL,
  `drug_purchase_number` int(10) UNSIGNED NOT NULL,
  `drug_purchase_price` int(10) UNSIGNED NOT NULL,
  `drug_purchase_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drug_purchase`
--

INSERT INTO `drug_purchase` (`drug_purchase_id`, `drug_id`, `drug_purchase_number`, `drug_purchase_price`, `drug_purchase_date`) VALUES
(1, 400001, 200, 50, '2017-03-03'),
(2, 400001, 100, 40, '2017-05-04'),
(3, 400006, 50, 200, '2017-01-24'),
(4, 400006, 300, 300, '2017-02-11'),
(5, 400001, 222, 50, '2017-11-11'),
(7, 400001, 222, 50, '2017-11-11');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(20) UNSIGNED NOT NULL,
  `petowner_id` int(20) NOT NULL,
  `invoice_date` date NOT NULL,
  `item_name` varchar(45) NOT NULL,
  `amount` int(20) NOT NULL,
  `invoice_description` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `petowner_id`, `invoice_date`, `item_name`, `amount`, `invoice_description`) VALUES
(1001, 20001, '2017-09-10', 'Doxycycline', 500, 'AAA');

-- --------------------------------------------------------

--
-- Table structure for table `pens`
--

CREATE TABLE `pens` (
  `pens_id` int(20) NOT NULL,
  `pens_state` int(2) NOT NULL DEFAULT '0',
  `pens_price` int(10) UNSIGNED NOT NULL,
  `pens_type` varchar(45) NOT NULL,
  `pens_location_description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pens`
--

INSERT INTO `pens` (`pens_id`, `pens_state`, `pens_price`, `pens_type`, `pens_location_description`) VALUES
(800001, 0, 200, 'cat', 'LOVEPETS1 lv2 room405');

-- --------------------------------------------------------

--
-- Table structure for table `pet`
--

CREATE TABLE `pet` (
  `pet_id` int(20) NOT NULL,
  `pet_name` varchar(45) NOT NULL,
  `pet_gender` char(6) NOT NULL,
  `pet_age` int(3) UNSIGNED NOT NULL,
  `pet_symptom` varchar(150) DEFAULT NULL,
  `pet_type` varchar(45) DEFAULT NULL,
  `pet_state` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet`
--

INSERT INTO `pet` (`pet_id`, `pet_name`, `pet_gender`, `pet_age`, `pet_symptom`, `pet_type`, `pet_state`) VALUES
(200001, 'Jill', 'female', 1, 'hurt', '1', b'1111111111111111111111111111111'),
(200002, 'Jim', 'male', 1, 'hurt', '1', b'1111111111111111111111111111111'),
(200003, 'Kit', 'female', 2, 'bleeding', '2', b'1111111111111111111111111111111'),
(200004, 'Lisa', 'female', 3, 'sick', '3', b'1111111111111111111111111111111'),
(200005, 'May', 'female', 1, 'sick', '1', b'1111111111111111111111111111111');

-- --------------------------------------------------------

--
-- Table structure for table `petowner`
--

CREATE TABLE `petowner` (
  `petowner_id` int(20) NOT NULL,
  `petowner_name` varchar(20) NOT NULL,
  `petowner_gender` char(6) NOT NULL,
  `petowner_address` varchar(120) NOT NULL,
  `petowner_contact number` int(20) UNSIGNED NOT NULL,
  `petowner_email` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `petowner`
--

INSERT INTO `petowner` (`petowner_id`, `petowner_name`, `petowner_gender`, `petowner_address`, `petowner_contact number`, `petowner_email`) VALUES
(20001, 'Dina', 'male', 'OWNER ROAD 100', 41310000, '041310000@gmail.com'),
(20002, 'Elvin', 'female', 'OWNER ROAD 200', 41320000, '041320000@gmail.com'),
(20003, 'Fear', 'male', 'OWNER ROAD 333', 41330000, '041330000@gmail.com'),
(20004, 'Hill', 'female', 'OWNER ROAD 450', 41340000, '041340000@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `petregistration`
--

CREATE TABLE `petregistration` (
  `id` int(11) NOT NULL,
  `first` varchar(256) NOT NULL,
  `last` varchar(256) NOT NULL,
  `pet` varchar(256) NOT NULL,
  `Date` date NOT NULL,
  `type` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `gender` varchar(256) NOT NULL,
  `message` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `petregistration`
--

INSERT INTO `petregistration` (`id`, `first`, `last`, `pet`, `Date`, `type`, `email`, `address`, `gender`, `message`) VALUES
(3, 'Vijay', 'Maharjan', 'alice', '0000-00-00', 'dog', 'vijay@gmail.com', '4/11 bell street', 'on', 'fever'),
(4, 'anita', 'ghale', 'kool', '2002-01-01', 'cat', 'anita@gmail.com', '4/11 kool street', 'on', 'dentistry');

-- --------------------------------------------------------

--
-- Table structure for table `pet_pens_ allocation`
--

CREATE TABLE `pet_pens_ allocation` (
  `pens_id` int(20) NOT NULL,
  `pet_id` int(20) NOT NULL,
  `pet_pens_ allocation_starttime` datetime NOT NULL,
  `pet_pens_ allocation_endtime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet_pens_ allocation`
--

INSERT INTO `pet_pens_ allocation` (`pens_id`, `pet_id`, `pet_pens_ allocation_starttime`, `pet_pens_ allocation_endtime`) VALUES
(800001, 200001, '2017-09-08 00:00:00', '2017-09-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pet_petowner`
--

CREATE TABLE `pet_petowner` (
  `pet_id` int(20) NOT NULL,
  `petowner_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet_petowner`
--

INSERT INTO `pet_petowner` (`pet_id`, `petowner_id`) VALUES
(200001, 20001),
(200002, 20001),
(200003, 20002),
(200004, 20003),
(200005, 20004);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(20) NOT NULL,
  `staff_name` varchar(45) NOT NULL,
  `staff_gender` char(6) NOT NULL,
  `staff_age` int(3) UNSIGNED NOT NULL,
  `position_name` varchar(45) NOT NULL,
  `staff_address` varchar(120) NOT NULL,
  `staff_contact_number` int(20) UNSIGNED NOT NULL,
  `staff_email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_name`, `staff_gender`, `staff_age`, `position_name`, `staff_address`, `staff_contact_number`, `staff_email`) VALUES
(100001, 'Tom', 'male', 18, 'doctor', 'PET ROAD 100', 41510001, '041510001@gmail.com'),
(100002, 'Tim', 'male', 32, 'nurse', 'PET ROAD 101', 41510002, '041510002@gmail.com'),
(100003, 'Bobi', 'male', 22, 'doctor', 'PET ROAD 200', 41510003, '041510003@gmail.com'),
(100004, 'Lia', 'female', 20, 'nurse', 'PET ROAD 100', 41510004, '041510004@gmail.com'),
(100005, 'Toby', 'male', 40, 'doctor', 'PET ROAD 450', 41510005, '041510005@gmail.com'),
(100006, 'Kelvin', 'male', 20, 'nurse', 'PET ROAD 332', 41510006, '041510006@gmail.com'),
(100007, 'Ruby', 'female', 18, 'nurse', 'PET ROAD 440', 41510007, '041510007@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `treatment`
--

CREATE TABLE `treatment` (
  `treatment_id` int(20) NOT NULL,
  `treatment_price` int(20) UNSIGNED NOT NULL,
  `treatment_description` varchar(45) DEFAULT NULL,
  `treatment_state` bit(1) NOT NULL,
  `treatment_type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `treatment`
--

INSERT INTO `treatment` (`treatment_id`, `treatment_price`, `treatment_description`, `treatment_state`, `treatment_type`) VALUES
(500100, 100, NULL, b'1111111111111111111111111111111', '1'),
(500101, 200, NULL, b'1111111111111111111111111111111', '2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_first` varchar(256) NOT NULL,
  `user_last` varchar(256) NOT NULL,
  `user_email` varchar(256) NOT NULL,
  `user_uid` varchar(256) NOT NULL,
  `user_pwd` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_first`, `user_last`, `user_email`, `user_uid`, `user_pwd`) VALUES
(8, 'Vijay', 'Maharjan', 'vijaymaharjan4@gmail.com', 'vijay', '$2y$10$JE3vDOfCoKtJTlKbtAmY4eoE6Rq1HPxQa93F6qqm7t5qVQmvake/u'),
(9, 'Anita', 'ghale', 'g@gmail.com', 'anita', '$2y$10$ka6En0Bc1EclkNMUL765G.Bt9te6MlLkPcn6qlQ.sTAfkgfILSl3K'),
(10, 'kool', 'kool', 'k@g.com', 'kool', '$2y$10$P1K9pfYlNv64s5QX8aNUROFtjTcCrF9cYNzQ6YLNaYXyO9Qp2nr16'),
(13, 'AAA', 'AAA', 'AAA@123.COM', 'AAAAAA', '$2y$10$xNgXJqxY5hLhLU4MVl0X9OnGXZrpZK6kdF3uu2gVUSyzQm3Nigyyy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`appointment_id`),
  ADD UNIQUE KEY `appointment_id_UNIQUE` (`appointment_id`),
  ADD KEY `staff_id_idx` (`staff_id`),
  ADD KEY `pet_id_idx` (`pet_id`);

--
-- Indexes for table `book_appointment`
--
ALTER TABLE `book_appointment`
  ADD PRIMARY KEY (`book_appointment_id`),
  ADD KEY `petowner_id66_idx` (`petowner_id`),
  ADD KEY `pet_id66_idx` (`pet_id`);

--
-- Indexes for table `clinic`
--
ALTER TABLE `clinic`
  ADD PRIMARY KEY (`clinic_id`),
  ADD UNIQUE KEY `clinic_id_UNIQUE` (`clinic_id`);

--
-- Indexes for table `clinic_staff`
--
ALTER TABLE `clinic_staff`
  ADD KEY `staff_id2` (`staff_id`),
  ADD KEY `clinic_id2` (`clinic_id`);

--
-- Indexes for table `drug_consumption`
--
ALTER TABLE `drug_consumption`
  ADD KEY `drug_id_idx` (`drug_id`),
  ADD KEY `clinic_id_idx` (`clinic_id`),
  ADD KEY `treatment_id_idx` (`treatment_id`);

--
-- Indexes for table `drug_inventory`
--
ALTER TABLE `drug_inventory`
  ADD PRIMARY KEY (`drug_id`),
  ADD UNIQUE KEY `drug_id_UNIQUE` (`drug_id`),
  ADD KEY `clinic_id16_idx` (`clinic_id`);

--
-- Indexes for table `drug_purchase`
--
ALTER TABLE `drug_purchase`
  ADD PRIMARY KEY (`drug_purchase_id`),
  ADD KEY `drug_id_idx` (`drug_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`),
  ADD UNIQUE KEY `invoice_id_UNIQUE` (`invoice_id`),
  ADD KEY `petowner_id0_idx` (`petowner_id`);

--
-- Indexes for table `pens`
--
ALTER TABLE `pens`
  ADD PRIMARY KEY (`pens_id`),
  ADD UNIQUE KEY `pens_id_UNIQUE` (`pens_id`);

--
-- Indexes for table `pet`
--
ALTER TABLE `pet`
  ADD PRIMARY KEY (`pet_id`),
  ADD UNIQUE KEY `pet_id_UNIQUE` (`pet_id`);

--
-- Indexes for table `petowner`
--
ALTER TABLE `petowner`
  ADD PRIMARY KEY (`petowner_id`),
  ADD UNIQUE KEY `petowner_id_UNIQUE` (`petowner_id`);

--
-- Indexes for table `petregistration`
--
ALTER TABLE `petregistration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_pens_ allocation`
--
ALTER TABLE `pet_pens_ allocation`
  ADD KEY `pens_id_idx` (`pens_id`),
  ADD KEY `pet_id_idx` (`pet_id`);

--
-- Indexes for table `pet_petowner`
--
ALTER TABLE `pet_petowner`
  ADD KEY `pet_id_idx` (`pet_id`),
  ADD KEY `petowner_id_idx` (`petowner_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`),
  ADD UNIQUE KEY `staff_id_UNIQUE` (`staff_id`);

--
-- Indexes for table `treatment`
--
ALTER TABLE `treatment`
  ADD PRIMARY KEY (`treatment_id`),
  ADD UNIQUE KEY `treatment_id_UNIQUE` (`treatment_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `appointment_id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5007;
--
-- AUTO_INCREMENT for table `book_appointment`
--
ALTER TABLE `book_appointment`
  MODIFY `book_appointment_id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=900004;
--
-- AUTO_INCREMENT for table `clinic`
--
ALTER TABLE `clinic`
  MODIFY `clinic_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300004;
--
-- AUTO_INCREMENT for table `drug_inventory`
--
ALTER TABLE `drug_inventory`
  MODIFY `drug_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=400009;
--
-- AUTO_INCREMENT for table `drug_purchase`
--
ALTER TABLE `drug_purchase`
  MODIFY `drug_purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1002;
--
-- AUTO_INCREMENT for table `pens`
--
ALTER TABLE `pens`
  MODIFY `pens_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=800002;
--
-- AUTO_INCREMENT for table `pet`
--
ALTER TABLE `pet`
  MODIFY `pet_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200006;
--
-- AUTO_INCREMENT for table `petowner`
--
ALTER TABLE `petowner`
  MODIFY `petowner_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20005;
--
-- AUTO_INCREMENT for table `petregistration`
--
ALTER TABLE `petregistration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100008;
--
-- AUTO_INCREMENT for table `treatment`
--
ALTER TABLE `treatment`
  MODIFY `treatment_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=500102;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `pet_id1` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`pet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `staff_id1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `book_appointment`
--
ALTER TABLE `book_appointment`
  ADD CONSTRAINT `pet_id66` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`pet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `petowner_id66` FOREIGN KEY (`petowner_id`) REFERENCES `petowner` (`petowner_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `clinic_staff`
--
ALTER TABLE `clinic_staff`
  ADD CONSTRAINT `clinic_id2` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`clinic_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `staff_id2` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drug_consumption`
--
ALTER TABLE `drug_consumption`
  ADD CONSTRAINT `clinic_id1` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`clinic_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `drug_id1` FOREIGN KEY (`drug_id`) REFERENCES `drug_inventory` (`drug_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `treatment_id2` FOREIGN KEY (`treatment_id`) REFERENCES `treatment` (`treatment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drug_inventory`
--
ALTER TABLE `drug_inventory`
  ADD CONSTRAINT `clinic_id16` FOREIGN KEY (`clinic_id`) REFERENCES `clinic` (`clinic_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `drug_purchase`
--
ALTER TABLE `drug_purchase`
  ADD CONSTRAINT `drug_id3` FOREIGN KEY (`drug_id`) REFERENCES `drug_inventory` (`drug_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `petowner_id0` FOREIGN KEY (`petowner_id`) REFERENCES `petowner` (`petowner_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pet_pens_ allocation`
--
ALTER TABLE `pet_pens_ allocation`
  ADD CONSTRAINT `pens_id4` FOREIGN KEY (`pens_id`) REFERENCES `pens` (`pens_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pet_id4` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`pet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pet_petowner`
--
ALTER TABLE `pet_petowner`
  ADD CONSTRAINT `pet_id6` FOREIGN KEY (`pet_id`) REFERENCES `pet` (`pet_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `petowner_id6` FOREIGN KEY (`petowner_id`) REFERENCES `petowner` (`petowner_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
