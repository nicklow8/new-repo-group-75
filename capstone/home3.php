<?php include "headsection1.php";?> <!--headsection included through php-->
<?php include "alertmessage.php"; ?> <!--pop up alert messages for users guide-->
<body>
    
   <div id="content" class="center">
    <h2>Quick Links</h2>
    <div id="links">
        
      <ul class="nomobile">  
         <li><a href="home3.php"><img src="image/home.png" width="100px" height="100px"><br>Home</a></li>
         <li><a href="registration.php"><img src="image/DOG.png" width="100px" height="100px"><br>Pet Registration</a></li>
         <li><a href="signup.php"><img src="image/user.png" width="100px" height="100px"><br>SignUp</a></li>
         <li><a href="staff_stock.php"><img src="image/warehouse.png" width="100px" height="100px"><br>Stock</a></li>
         <li><a href="staff edit.php"><img src="image/add.png" width="100px" height="100px"><br>Add Staff</a></li>
         <li><a href="bookingdetails.php"><img src="image/appo.png" width="100px" height="100px"><br>Appointment</a></li>
         <li><a href="clinic_pens.php"><img src="image/pen.png" width="100px" height="100px"><br>Clinic Pen</a></li>
         <li><a href="pagination.php"><img src="image/catview.png" width="100px" height="100px"><br>Pet Record</a></li>
         <li><a href="table_staff.php"><img src="image/nurse.png" width="100px" height="100px"><br>Staff Record</a></li>
        <!-- <li><a href="view.php"><img src="image/female.png" width="100px" height="100px"><br>User Record</a></li>-->
         <li><a href="deletedusersdata.php"><img src="image/Denied.png" width="100px" height="100px"><br>Deleted Staff </a></li>
         <li><a href="deletedpet.php"><img src="image/petdelete.png" width="100px" height="100px"><br>Deleted Pet</a></li>
        <!-- <li><a href="deletedusersdata.php"><img src="image/delete1.png" width="100px" height="100px"><br>Deleted User</a></li>-->
      </ul>
      
    </div>
   
   </div>
      
</body>

<?php include "footer.php" ?>     <!-- footer included through php-->
</html>