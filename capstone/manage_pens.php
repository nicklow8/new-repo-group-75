﻿
<?php include "headsection1.php" ?>
<?php require "./includes/search.inc" ?>
<script>
function goBack() {
    window.history.back()
}
</script>

<body>
      <div id="content" class="center">

        <h2>Welcome Staff!</h2>
        <br>
        <p>Changes are automatically saved to the database, please make changes carefully and click the 'Return' button to go back.</p>
          <div id="content">
            <table>
              <th>Pen ID</th>
              <th>Pet</th>
              <th>Availability</th>
              <th>Pen Date Available</th>
              <th>Clinic</th>
              <?php
               //print_r($results)
              foreach($results as $item) {
                echo '<tr>';
                echo '<td class="pen-id">' . $item[0] . '</a></td>';
                echo '<td><select class="update-animal" rel="'.$item[0].'">';
                if ($item[1] == "Dog") {
                echo '<option selected disabled>Dog</option><option value="Cat">Cat</option><option value="None">None</option></select></td>';

              } elseif ($item[1] == "Cat") {
                echo  '<option selected disabled>Cat</option><option value="Dog">Dog</option><option value="None">None</option></select></td>';

                } else {
                echo '<option selected disabled>None</option><option value="Dog">Dog</option><option value="Cat">Cat</option></select></td>';

                }
                echo '<td><select class="update-availability" rel="'.$item[0].'">';
                if ($item[2] == "Available") {
                  echo '<option selected disabled>Available</option><option value="Unavailable">Unavailable</option></select></td>';
                }
                elseif ($item[2] == "Unavailable") {
                  echo '<option selected disabled>Unavailable</option><option value="Available">Available</option></select></td>';
                }
                // echo $Date = "17-10-2017";
                // echo date('d-m-y', strtotime($item[3]. ' + 1 days'));
                // echo date('d-m-y', strtotime($Date. ' + 2 days'));
                // echo '<td>' .date('d-m-y', strtotime($item[3]. ' + 1 days')) . '</td>';
                echo '<td><select class="update-date-availability" rel="'.$item[0].'">';

                if ($item[2] == "Available") {
                  for ($i = 0; $i < 30; $i++) {
                    echo '<option value='.date('Y-m-d', strtotime(gmdate('Y-m-d h:i:s') . " + $i days")).'>' . date('Y-m-d', strtotime(gmdate('Y-m-d h:i:s') . " + $i days")) . "</option>";
                  }
                } else {
                  for ($i = 0; $i < 30; $i++) {
                    $date = date('Y-m-d', strtotime(gmdate('Y-m-d h:i:s') . " + $i days"));
                    if ($date == date("Y-m-d", strtotime($item[3]))) {
                        echo '<option selected value='.$date.'>' . $date . "</option>";
                    } else {
                        echo '<option value='.$date.'>' . $date . "</option>";
                    }

                  }
                }

                 echo '</select>';
                 echo '</td>';
                echo '<td>' . $item[4] . '</td>';
                echo '</tr>';
              }
              ?>

          </table>
            <div id="map-image">

                         </div>
                                  <br>
                <button onclick="goBack()">Return to Pen Availability Page</button>
                <br>
                  <hr>
                   <br>

                           <?php include "footer.php" ?>     <!-- footer included through php-->
    </div>
  </div>
</body>
</html>
